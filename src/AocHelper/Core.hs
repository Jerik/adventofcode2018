{-# LANGUAGE TypeFamilies, FlexibleContexts #-}

module AocHelper.Core where

import Control.Monad (forM_)
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Trans.Maybe (MaybeT)

class (Show (OutputPart1 d), Show (OutputPart2 d)) => Day d where
    type Input d
    type OutputPart1 d
    type OutputPart2 d
    getNumber    :: d -> Int
    getSolutions :: d -> (Maybe (OutputPart1 d), Maybe (OutputPart2 d))
    createDay    :: Input d -> MaybeT IO d
    printDay     :: d -> MaybeT IO ()
    printDay day = liftIO $ do
        putStrLn $ "Day " ++ show (getNumber day) ++ ":"
        let (sol1, sol2) = getSolutions day
        forM_ sol1 $ putStrLn . ("Part 1: " ++) . show
        forM_ sol2 $ putStrLn . ("Part 2: " ++) . show
        putStrLn ""
