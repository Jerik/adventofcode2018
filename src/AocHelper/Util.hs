{-# LANGUAGE TupleSections, ScopedTypeVariables #-}

module AocHelper.Util where

import Data.Function (on)
import Data.List (maximumBy, tails)
import Data.List.NonEmpty (nonEmpty)
import qualified Data.Map.Strict as Map
import Data.Map.Strict (Map)
import Data.Set (Set)

eitherToMaybe :: Either a b -> Maybe b
eitherToMaybe = either (const Nothing) Just

getPairs :: [a] -> [(a, a)]
getPairs as = do
    (a:as') <- tails as
    a' <- as'
    return (a, a')

getDuplicates :: Ord a => [a] -> Set a
getDuplicates = Map.keysSet . Map.filter (>=2) . Map.fromListWith (+) . map (,1)

safeMapMaximumBy :: (a -> a -> Ordering) -> Map k a -> Maybe (k, a)
safeMapMaximumBy f = fmap (maximumBy (f `on` snd)) . nonEmpty . Map.toList

safeMapMaximum :: Ord a => Map k a -> Maybe (k, a)
safeMapMaximum = safeMapMaximumBy compare

uniqueMinimumBy :: forall a. (a -> a -> Ordering) -> [a] -> Maybe a
uniqueMinimumBy _ [] = Nothing
uniqueMinimumBy cmp (a:as) = if unique then Just min else Nothing where
    (min,unique) = foldl updateMin (a,True) as
    updateMin :: (a,Bool) -> a -> (a,Bool)
    updateMin (min,unique) a = case cmp min a of
        LT -> (min,unique)
        EQ -> (min,False)
        GT -> (a,True)
