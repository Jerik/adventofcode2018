module AocHelper.Parse where

import AocHelper.Core
import Data.Either (isRight)
import Control.Monad (guard)
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Trans.Maybe (MaybeT)
import Text.Parsec (many1, optionMaybe)
import Text.Parsec.Char (digit, oneOf)
import Text.Parsec.String (Parser, parseFromFile)

parseDayFromFile :: (Day d) => Parser d -> FilePath -> MaybeT IO d
parseDayFromFile parser path = do
    result <- liftIO $ parseFromFile parser path
    guard $ isRight result
    let Right day = result
    return day

number :: Parser Int
number = do
    sign <- optionMaybe $ oneOf "+-"
    digits <- many1 digit
    let n = read digits
    return $ case sign of Just '-' -> negate n; _ -> n
