{-# LANGUAGE TypeFamilies, TupleSections #-}

module Days.Day2 (Day2) where

import AocHelper.Core
import AocHelper.Parse (parseDayFromFile)
import AocHelper.Util (getPairs)
import Control.Monad (msum)
import Data.List (partition)
import qualified Data.Map.Strict as Map
import Data.Maybe (isJust)
import Text.Parsec (many1, sepEndBy1)
import Text.Parsec.Char (letter, newline)
import Text.Parsec.String (Parser)

-- Types
--
newtype Day2 = Day2 { boxIDs :: [String] }

-- Day logic
--
instance Day Day2 where
    type Input Day2 = FilePath
    type OutputPart1 Day2 = Int
    type OutputPart2 Day2 = String
    getNumber = const 2
    getSolutions (Day2 bids) = (Just $ calcChecksum bids, msum $ map (uncurry compareBoxIDs) (getPairs bids))
    createDay = parseDayFromFile day2

calcChecksum :: [String] -> Int
calcChecksum = (\(xs, ys) -> countTrue xs * countTrue ys) . unzip . map checkBoxID where
    countTrue :: [Bool] -> Int
    countTrue = length . filter id
    checkBoxID :: String -> (Bool, Bool)
    checkBoxID bid = (2 `elem` occs, 3 `elem` occs) where
        occs = Map.elems $ Map.fromListWith (+) (map (,1) bid)

compareBoxIDs :: String -> String -> Maybe String
compareBoxIDs bid1 bid2 = if length uncommon == 1 then sequence common else Nothing where
    (common, uncommon) = partition isJust $ zipWith (\ch1 ch2 -> if ch1 == ch2 then Just ch1 else Nothing) bid1 bid2

-- Parsers
--
day2 :: Parser Day2
day2 = Day2 <$> many1 letter `sepEndBy1` newline
