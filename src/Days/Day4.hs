{-# LANGUAGE TypeFamilies, TupleSections #-}

module Days.Day4 (Day4) where

import AocHelper.Core
import AocHelper.Parse (number, parseDayFromFile)
import AocHelper.Util (eitherToMaybe, safeMapMaximum, safeMapMaximumBy)
import Data.List (sortOn)
import qualified Data.Map.Strict as Map
import Data.Map.Strict (Map)
import Data.Ord (comparing)
import Text.Parsec (Parsec, (<|>), anyToken, between, lookAhead, many, runParser, sepEndBy1, try)
import Text.Parsec.Char (char, string, space, newline)
import Text.Parsec.String (Parser)

-- Types
--
newtype Day4 = Day4 { records :: [Record] }

data Record = Record { recordTime :: Time, recordAction :: Action } deriving (Show)
data Time = Time { year, month, day, hour :: Int, minute :: Minute } deriving (Show, Eq, Ord)
data Action = Begin { guard :: Guard } | Pause | Resume deriving (Show)
newtype Guard = Guard { guardID :: Int } deriving (Show, Eq, Ord)

type ParserRc = Parsec [Record] ()
type Minute = Int
type TimeTable = Map Minute Int

-- Day logic
--
instance Day Day4 where
    type Input Day4 = FilePath
    type OutputPart1 Day4 = Int
    type OutputPart2 Day4 = Int
    getNumber = const 4
    getSolutions (Day4 rcs) = (tables >>= strategy1, tables >>= strategy2) where
        tables = eitherToMaybe $ runParser timeTables () "" (sortOn recordTime rcs)
    createDay = parseDayFromFile day4

strategy1 :: Map Guard TimeTable -> Maybe Int
strategy1 tables = do
    (lazyGuard, table) <- safeMapMaximumBy (comparing $ sum . Map.elems) tables
    lazyMinute <- fst <$> safeMapMaximum table
    return $ guardID lazyGuard * lazyMinute

strategy2 :: Map Guard TimeTable -> Maybe Int
strategy2 tables = do
    let maxTable = Map.mapMaybe id $ fmap safeMapMaximum tables
    (lazyGuard, (lazyMinute, _)) <- safeMapMaximumBy (comparing snd) maxTable
    return $ guardID lazyGuard * lazyMinute

-- Parsers
--
day4 :: Parser Day4
day4 = Day4 <$> record `sepEndBy1` newline

record :: Parser Record
record = Record <$> between (char '[') (char ']') time <* space <*> action

time :: Parser Time
time = Time <$> number <* char '-' <*> number <* char '-' <*> number <* space <*> number <* char ':' <*> number

action :: Parser Action
action = Begin . Guard <$ string "Guard #" <*> number <* string " begins shift"
    <|> Pause <$ string "falls asleep"
    <|> Resume <$ string "wakes up"

timeTables :: ParserRc (Map Guard TimeTable)
timeTables = do
    shifts <- many shift
    return $ Map.fromListWith (Map.unionWith (+)) shifts

shift :: ParserRc (Guard, TimeTable)
shift = do
    guardOnShift <- guard . recordAction <$> beginRecord
    intervals <- many $ try pauseInterval
    return (guardOnShift, Map.fromDistinctAscList $ intervals >>= map (,1))

pauseInterval :: ParserRc [Minute]
pauseInterval = do
    startMinute <- minute . recordTime <$> pauseRecord
    endMinute <- minute . recordTime <$> (resumeRecord <|> lookAhead beginRecord)
    return [startMinute..endMinute-1]

beginRecord :: ParserRc Record
beginRecord = do rc@(Record _ (Begin _)) <- anyToken; return rc

pauseRecord :: ParserRc Record
pauseRecord = do rc@(Record _ Pause) <- anyToken; return rc

resumeRecord :: ParserRc Record
resumeRecord = do rc@(Record _ Resume) <- anyToken; return rc
