{-# LANGUAGE TypeFamilies #-}

module Days.Day1 (Day1) where

import AocHelper.Core
import AocHelper.Parse (number, parseDayFromFile)
import qualified Data.IntSet as IS
import Data.IntSet (IntSet)
import Text.Parsec (sepEndBy1)
import Text.Parsec.Char (newline)
import Text.Parsec.String (Parser)

-- Types
--
newtype Day1 = Day1 { frequencyChanges :: [Int] }

-- Day logic
--
instance Day Day1 where
    type Input Day1 = FilePath
    type OutputPart1 Day1 = Int
    type OutputPart2 Day1 = Int
    getNumber = const 1
    getSolutions (Day1 fcs) = (Just $ sum fcs, Just $ findRepeat fcs)
    createDay = parseDayFromFile day1

findRepeat :: [Int] -> Int
findRepeat []  = 0
findRepeat fcs = loop 0 (IS.singleton 0) (cycle fcs) where
    loop :: Int -> IntSet -> [Int] -> Int
    loop frequency history (fc:fcs) = let newFrequency = frequency + fc in
        if newFrequency `IS.member` history then newFrequency
        else loop newFrequency (IS.insert newFrequency history) fcs

-- Parsers
--
day1 :: Parser Day1
day1 = Day1 <$> number `sepEndBy1` newline
