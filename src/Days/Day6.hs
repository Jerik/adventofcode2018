{-# LANGUAGE TypeFamilies, TupleSections #-}

module Days.Day6 (Day6) where

import AocHelper.Core
import AocHelper.Parse (number, parseDayFromFile)
import AocHelper.Util (safeMapMaximum, uniqueMinimumBy)
import qualified Data.Map.Strict as Map
import Data.Map.Strict (Map)
import Data.Ord (comparing)
import Text.Parsec (sepEndBy1)
import Text.Parsec.Char (newline, string)
import Text.Parsec.String (Parser)

-- Types
--
newtype Day6 = Day6 { points :: [Point] }

data Point = Point { x, y :: Int } deriving (Eq, Ord)
data Rectangle = Rectangle { lowCorner, highCorner :: Point }

-- Day logic
--
instance Day Day6 where
    type Input Day6 = FilePath
    type OutputPart1 Day6 = Int
    type OutputPart2 Day6 = Int
    getNumber = const 6
    getSolutions (Day6 pts) = (calcLargestAreaSize pts, calcSafeRegionSize pts)
    createDay = parseDayFromFile day6

calcLargestAreaSize :: [Point] -> Maybe Int
calcLargestAreaSize [] = Nothing
calcLargestAreaSize pts = safeMapMaximum areaSizes >>= snd where
    areaSizes = foldl updateAreaSizes (Map.fromList $ map (,Just 0) pts) (rectanglePoints boundingRect)
    boundingRect = boundingRectangle pts
    updateAreaSizes :: Map Point (Maybe Int) -> Point -> Map Point (Maybe Int)
    updateAreaSizes sizeMap pt = case nearestNeighbour pt $ Map.keys sizeMap of
        Just neighbour -> Map.adjust updateSize neighbour sizeMap where
            updateSize :: Maybe Int -> Maybe Int
            updateSize = if onBorder boundingRect pt then const Nothing else fmap (+1)
        Nothing -> sizeMap

-- Remark: In general, not all solutions lie within the bounding rectangle.
calcSafeRegionSize :: [Point] -> Maybe Int
calcSafeRegionSize [] = Nothing
calcSafeRegionSize pts = Just $ length (filter (<=10000) $ map totalDistance boundingRect) where
    boundingRect = rectanglePoints $ boundingRectangle pts
    totalDistance :: Point -> Int
    totalDistance = sum . flip map pts . manhattanDistance

manhattanDistance :: Point -> Point -> Int
manhattanDistance (Point x1 y1) (Point x2 y2) = abs (x2 - x1) + abs (y2 - y1)

nearestNeighbour :: Point -> [Point] -> Maybe Point
nearestNeighbour = uniqueMinimumBy . comparing . manhattanDistance

boundingRectangle :: [Point] -> Rectangle
boundingRectangle [] = error "Empty point-set"
boundingRectangle pts = Rectangle (Point xMin yMin) (Point xMax yMax) where
    xMin = minimum $ map x pts; xMax = maximum $ map x pts
    yMin = minimum $ map y pts; yMax = maximum $ map y pts

rectanglePoints :: Rectangle -> [Point]
rectanglePoints (Rectangle (Point x1 y1) (Point x2 y2)) = [Point i j | i <- [x1..x2], j <- [y1..y2]]

onBorder :: Rectangle -> Point -> Bool
onBorder (Rectangle lc hc) pt = x pt `elem` [x lc,x hc] || y pt `elem` [y lc,y hc]

-- Parsers
--
day6 :: Parser Day6
day6 = Day6 <$> point `sepEndBy1` newline

point :: Parser Point
point = Point <$> number <* string ", " <*> number
