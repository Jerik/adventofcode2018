{-# LANGUAGE TypeFamilies #-}

module Days.Day5 (Day5) where

import AocHelper.Core
import AocHelper.Parse (parseDayFromFile)
import Data.Char (isLower, toLower)
import qualified Data.Set as Set
import Text.Parsec (many1)
import Text.Parsec.Char (letter)
import Text.Parsec.String (Parser)

-- Types
--
newtype Day5 = Day5 { polymer :: String }

-- Day logic
--
instance Day Day5 where
    type Input Day5 = FilePath
    type OutputPart1 Day5 = Int
    type OutputPart2 Day5 = Int
    getNumber = const 5
    getSolutions (Day5 pol) = (Just $ react pol, improveReaction pol)
    createDay = parseDayFromFile day5

react :: String -> Int
react = length . foldl processUnit "" where
    processUnit :: String -> Char -> String
    processUnit (u:us) u' | toLower u == toLower u' && isLower u /= isLower u' = us
    processUnit us u = u:us

improveReaction :: String -> Maybe Int
improveReaction pol = Set.lookupMin $ Set.map reactWithout (Set.fromList $ map toLower pol) where
    reactWithout :: Char -> Int
    reactWithout u = react $ filter ((/=u) . toLower) pol

-- Parsers
--
day5 :: Parser Day5
day5 = Day5 <$> many1 letter
