{-# LANGUAGE TypeFamilies #-}

module Days.Day3 (Day3) where

import AocHelper.Core
import AocHelper.Parse (number, parseDayFromFile)
import AocHelper.Util (getDuplicates)
import Control.Monad (msum)
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import Data.Set (Set)
import Text.Parsec (sepEndBy1)
import Text.Parsec.Char (char, newline, string)
import Text.Parsec.String (Parser)

-- Types
--
newtype Day3 = Day3 { claims :: [Claim] }

data Claim = Claim { claimID, x, y, width, height :: Int }

-- Day logic
--
instance Day Day3 where
    type Input Day3 = FilePath
    type OutputPart1 Day3 = Int
    type OutputPart2 Day3 = Int
    getNumber = const 3
    getSolutions (Day3 cls) = (Just $ length overlaps, msum $ map (checkIntact overlaps) cls) where
        overlaps = getDuplicates $ cls >>= getClaimedPositions
    createDay = parseDayFromFile day3

getClaimedPositions :: Claim -> [(Int, Int)]
getClaimedPositions (Claim _ x y w h) = [(i, j) | i <- [x..x+w-1], j <- [y..y+h-1]]

checkIntact :: Set (Int, Int) -> Claim -> Maybe Int
checkIntact overlaps cl = if Set.disjoint overlaps (Set.fromList $ getClaimedPositions cl)
    then Just $ claimID cl else Nothing

-- Parsers
--
day3 :: Parser Day3
day3 = Day3 <$> claim `sepEndBy1` newline

claim :: Parser Claim
claim = Claim <$ char '#' <*> number <* string " @ "
    <*> number <* char ',' <*> number <* string ": " <*> number <* char 'x' <*> number
