module Main where

import AocHelper.Core
import Control.Monad.Trans.Maybe (MaybeT, runMaybeT)
import Days

main :: IO (Maybe ())
main = do
    runMaybeT $ (createDay "input_files/input1.txt" :: MaybeT IO Day1) >>= printDay
    runMaybeT $ (createDay "input_files/input2.txt" :: MaybeT IO Day2) >>= printDay
    runMaybeT $ (createDay "input_files/input3.txt" :: MaybeT IO Day3) >>= printDay
    runMaybeT $ (createDay "input_files/input4.txt" :: MaybeT IO Day4) >>= printDay
    runMaybeT $ (createDay "input_files/input5.txt" :: MaybeT IO Day5) >>= printDay
    runMaybeT $ (createDay "input_files/input6.txt" :: MaybeT IO Day6) >>= printDay
